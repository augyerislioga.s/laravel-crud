@extends('layout.master')

@section('judul')
Sign Up Form
@endsection

@section('isi')
    <form action="/post" method="POST">
        @csrf
        <label>First name</label><br>
        <input type="text" name="firstname"> <br>
        <label>Last name</label><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender</label><br> <br>
        <input type="radio" name="Male"> Male <br>
        <input type="radio" name="Female"> Female <br>
        <input type="radio" name="Other"> Other <br><br>
        <label> Nationality</label> <br><br>
        <select name="nation">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australian">Australian</option>
        </select> <br>
        <br><label>Language Spoken:</label><br>
        <br><input type="checkbox" value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" value="English"> English <br>
        <input type="checkbox" value="Other"> Other <br>
        <label>bio</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <br><input type="submit" value="Kirim">
    </form>
        
@endsection